# app-nodejs

此範例程式碼使用 NodeJS + Express 撰寫，該範例提供一個簡易的使用者登入系統。

## 參考文件
* [建立 Docker 的開發環境](./docker/setup-dev-env.md)

## 應用服務頁面操作

1. 前往首頁 (http://localhost:3000), 頁面將自動導向使用者登入畫面 (http://localhost:3000/login)

   <img src="./_images/login-page.png" width="50%">
2. 輸入使用者資訊
   - 在密碼欄位輸入 `strongpassword`（僅會檢查密碼，帳號為任意輸入）：顯示歡迎訊息與應用程式的資訊

     <img src="./_images/login-success.png" width="50%">
   - 若密碼錯誤，則會停留在登入畫面並看到資訊錯誤的提醒

     <img src="./_images/login-failed.png" width="50%">

**備註：** 若非在本機端運行此範例，請將 `localhost` 替換成該程式運行的主機 IP


<h2> :warning: 注意事項！為進行教學，此程式碼隱含不安全設計的範例，請勿直接使用於您的正式環境！</h2>
