const path = require('path');
const express = require('express');
const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.set('port', (process.env.PORT || 3000))
app.use(express.static(__dirname + '/public'))

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.get('/', function(req, res) {
  return res.redirect('/login')
})

const login = require('./src/login');
app.use('/login', function(req, res, next) {
  if (req.method == 'GET') {
    res.render('login');
  } else {
    if (login.checkPsw(req.body.password)) {
      let appinfo = require('./package.json');
      let helloMsg = login.getHelloMsg(req.body.username);
      return res.status(200).render('home', {
        msg: helloMsg,
        env: process.env.NODE_ENV,
        ver: appinfo.version
      });
    } else {
      return res.status(401).render('login', {
        error_msg: "Wrong username or password. Try again!"
      });
    }
  }
})

const users = require('./src/users');
app.use('/profile', async function(req, res, next) {
  if (req.method == 'GET') {
    res.render('profile');
  } else {
    try {
      profile = await users.getProfile(req.body.username);
      console.log('profile', profile);
      res.json(profile);
    } catch (err) {
      res.status(503).render('error', {
        error_msg: '503 Service Unavailable',
        error_description: 'The server is currently unavailable.'
      });
    }
  }
})

app.listen(app.get('port'), function() {
  console.log("Node app is running at localhost:" + app.get('port'));
})

module.exports = app