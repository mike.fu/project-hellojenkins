pipeline {
    agent any

    environment {
        VERSION = sh(
                returnStdout: true,
                script: "grep version package.json | awk '{print \$2}' | sed  's/[\"|,]//g'"
            )
    }

    stages {
        stage('build') {
            steps {
                sh 'echo "Building.., app version: $VERSION"'
                sh 'docker build --no-cache -t $JOB_NAME:$BUILD_NUMBER .'
            }
        }

        stage('secure_test') {
            parallel {
                stage('secret_detection') {
                    steps {
                        echo 'Running secret detection..'
                        // Failed if files contain sensitive data
                        sh 'docker run --rm -v ${PWD}:/code ghcr.io/gitleaks/gitleaks:latest detect --no-git --source="/code" -v'
                    }
                }

                stage('container_scanning') {
                    steps {
                        echo 'Running container scanning..'
                        // Failed if container (scope: OS and application packages) has HIGH or CRITICAL vulnerabilities
                        sh '''
                            docker run --rm -v /var/run/docker.sock:/var/run/docker.sock \
                                aquasec/trivy --exit-code 1 --severity HIGH,CRITICAL --scanners vuln \
                                image $JOB_NAME:$BUILD_NUMBER
                        '''
                    }
                }

                stage('sast') {
                    steps {
                        echo 'Running SAST w/ SonarQube..'
                        // Failed if violating Quality Gates
                        withCredentials([usernamePassword(credentialsId: 'sq-mikefu-project', usernameVariable: 'SONAR_KEY', passwordVariable: 'SONAR_TOKEN')]) {
                            sh '''
                                docker run \
                                    --rm \
                                    --network=host \
                                    -e SONAR_HOST_URL="http://140.119.162.138:9000"  \
                                    -e SONAR_SCANNER_OPTS="-Dsonar.projectKey=$SONAR_KEY -Dsonar.exclusions=tests/** -Dsonar.qualitygate.wait=true" \
                                    -e SONAR_TOKEN=$SONAR_TOKEN \
                                    -v "${PWD}:/usr/src" \
                                    sonarsource/sonar-scanner-cli
                            '''
                        }
                    }
                }
            }
        }
    }

    post {
        cleanup {
            echo 'Cleanup..'
            sh 'docker rmi $JOB_NAME:$BUILD_NUMBER'
        }
    }
}
